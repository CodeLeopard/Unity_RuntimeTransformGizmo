﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/




using System;

using UnityEngine;

namespace RuntimeGizmos
{
	/// <summary>
	/// Component that will trigger events when it is interacted with by the <see cref="TransformGizmo"/>.
	/// </summary>
	[DisallowMultipleComponent]
	public class TransformGizmoEventReceiver : MonoBehaviour
	{
		/// <summary>
		/// Is Movement allowed for this object, and on which <see cref="Axis"/>.
		/// </summary>
		public Axis AllowedMovementAxis = Axis.All;

		/// <summary>
		/// Is Rotation allowed for this object, and around which <see cref="Axis"/>.
		/// </summary>
		public Axis AllowedRotateAxis = Axis.All;

		/// <summary>
		/// Is Scale allowed for this object, and on which <see cref="Axis"/>.
		/// </summary>
		public Axis AllowedScaleAxis = Axis.All;

		/// <summary>
		/// Is Raycast dragging allowed for this object.
		/// </summary>
		public bool AllowRaycastMove = true;

		/// <summary>
		/// The LayerMask to use when using <see cref="TransformType.RayCast"/>.
		/// </summary>
		public LayerMask RayCastLayerMask = Physics.DefaultRaycastLayers;

		/// <summary>
		/// Flags for the Pivot Points that are allowed to be used on this object.
		/// </summary>
		public TransformPivot AllowedPivot = TransformPivot.All;


		/// <summary>
		/// <see langword="event"/> that is triggered when the <see cref="TransformGizmo"/> is attached to this <see cref="GameObject"/>.
		/// </summary>
		// The transform parameter is for when the event is consumed by a manager which is responsible for multiple objects, and needs to know which one.
		[field: NonSerialized]
		public event Action<Transform> GizmoAttach;

		/// <summary>
		/// <see langword="event"/> that is triggered when the <see cref="TransformGizmo"/>'s handle is clicked.
		/// </summary>
		// The transform parameter is for when the event is consumed by a manager which is responsible for multiple objects, and needs to know which one.
		[field: NonSerialized]
		public event Action<Transform> GizmoStartInteract;

		/// <summary>
		/// <see langword="event"/> that is triggered every Update while the <see cref="TransformGizmo"/>'s handle is held.
		/// </summary>
		// The transform parameter is for when the event is consumed by a manager which is responsible for multiple objects, and needs to know which one.
		[field: NonSerialized]
		public event Action<Transform> GizmoInteract;


		/// <summary>
		/// <see langword="event"/> that is triggered when the <see cref="TransformGizmo"/>'s handle is released.
		/// </summary>
		// The transform parameter is for when the event is consumed by a manager which is responsible for multiple objects, and needs to know which one.
		[field: NonSerialized]
		public event Action<Transform> GizmoEndInteract;

		/// <summary>
		/// <see langword="event"/> that is triggered when the <see cref="TransformGizmo"/> is detached from this <see cref="GameObject"/>.
		/// </summary>
		// The transform parameter is for when the event is consumed by a manager which is responsible for multiple objects, and needs to know which one.
		[field: NonSerialized]
		public event Action<Transform> GizmoDetach;


		/// <summary>
		/// <see langword="event"/> that is triggered just before the <see cref="TransformGizmo"/> deletes this <see cref="GameObject"/>.
		/// Note that in the case of multiple selected objects the <see langword="event"/> will fire on all of them before any object is deleted.
		/// </summary>
		// The transform parameter is for when the event is consumed by a manager which is responsible for multiple objects, and needs to know which one.
		[field: NonSerialized]
		public event Action<Transform> GizmoDelete;


		public void FireGizmoAttach()
		{
			GizmoAttach?.Invoke(transform);
		}

		public void FireGizmoStartInteract()
		{
			GizmoStartInteract?.Invoke(transform);
		}

		public void FireGizmoInteract()
		{
			GizmoInteract?.Invoke(transform);
		}

		public void FireGizmoEndInteract()
		{
			GizmoEndInteract?.Invoke(transform);
		}

		public void FireGizmoDetach()
		{
			GizmoDetach?.Invoke(transform);
		}

		// todo: This does not belong here (separation of concerns).
		/// <summary>
		/// If multiple items are selected the event must fire on all of them before any is deleted.
		/// </summary>
		public void FireGizmoDelete()
		{
			GizmoDelete?.Invoke(transform);
		}
	}
}
