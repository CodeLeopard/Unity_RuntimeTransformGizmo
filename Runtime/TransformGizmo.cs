// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


// This file is a modification of the original, which has the following copyright:
// Copyright (c) 2016 HiddenMonk
// The Original, licensed MIT can be found here: https://github.com/HiddenMonk/Unity3DRuntimeTransformGizmo



using System;
using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

using CommandUndoRedo;

using Shared;

namespace RuntimeGizmos
{
	// To be safe, if you are changing any transforms hierarchy, such as parenting an object to something,
	// you should call ClearTargets before doing so just to be sure nothing unexpected happens...
	// as well as call UndoRedoManager.Clear()
	// For example, if you select an object that has children, move the children elsewhere, deselect the original object, then try to add those old children to the selection, I think it wont work.

	[DisallowMultipleComponent]
	[RequireComponent(typeof(Camera))]
	public class TransformGizmo : MonoBehaviour
	{
		#region Public fields
		#region Controls

		public TransformSpace space = TransformSpace.Global;
		public TransformType transformType = TransformType.Move;
		public TransformPivot pivot = TransformPivot.Origin;
		public CenterType centerType = CenterType.All;
		public ScaleType scaleType = ScaleType.FromPoint;

		public KeyCode SetNoneType = KeyCode.Q;
		// These are the same as the unity editor hotkeys
		public KeyCode SetMoveType = KeyCode.W;
		public KeyCode SetRotateType = KeyCode.E;
		public KeyCode SetScaleType = KeyCode.R;
		public KeyCode SetRayCastType = KeyCode.T;
		public KeyCode SetAllTransformType = KeyCode.Y;
		public KeyCode SetSpaceToggle = KeyCode.X;
		public KeyCode SetPivotModeToggle = KeyCode.Z;
		public KeyCode SetCenterTypeToggle = KeyCode.C;
		public KeyCode SetScaleTypeToggle = KeyCode.S;
		public KeyCode translationSnapping = KeyCode.LeftControl;
		public KeyCode AddSelection = KeyCode.LeftShift;
		public KeyCode RemoveSelection = KeyCode.LeftControl;
		// It's set to shift instead of control so that while in the editor we don't accidentally undo editor changes =/
		public KeyCode ActionKey = KeyCode.LeftShift;
		public KeyCode UndoAction = KeyCode.Z;
		public KeyCode RedoAction = KeyCode.Y;

		#endregion Controls

		/// <summary>
		/// A function that replaces the default selection logic.
		/// </summary>
		public Action CustomGetTarget;
		/// <summary>
		/// <see langword="event"/> that fires when the selection has changed.
		/// </summary>
		[field: NonSerialized] // Unity
		public event Action OnSelectionChanged;

		/// <summary>
		/// A function that checks if Keyboard interaction is allowed.
		/// If it is <see langword="null"/> keyboard interaction will always be allowed.
		/// </summary>
		public Func<bool> AllowKeyboardInteraction;

		#region Visual Settings

		public Color xColor = new Color(1, 0, 0, 0.8f);
		public Color yColor = new Color(0, 1, 0, 0.8f);
		public Color zColor = new Color(0, 0, 1, 0.8f);
		public Color allColor = new Color(.7f, .7f, .7f, 0.8f);
		public Color selectedColor = new Color(1, 1, 0, 0.8f);
		public Color hoverColor = new Color(1, .75f, 0, 0.8f);
		public float planesOpacity = .5f;
		//public Color rectPivotColor = new Color(0, 0, 1, 0.8f);
		//public Color rectCornerColor = new Color(0, 0, 1, 0.8f);
		//public Color rectAnchorColor = new Color(.7f, .7f, .7f, 0.8f);
		//public Color rectLineColor = new Color(.7f, .7f, .7f, 0.8f);

		public float movementSnap = .25f;
		public float rotationSnap = 15f;
		public float scaleSnap = 1f;

		public float handleLength = .25f;
		public float handleWidth = .003f;
		public float planeSize = .035f;
		public float triangleSize = .03f;
		public float boxSize = .03f;
		public int circleDetail = 40;
		public float allMoveHandleLengthMultiplier = 1f;
		public float allRotateHandleLengthMultiplier = 1.4f;
		public float allScaleHandleLengthMultiplier = 1.6f;
		public float minSelectedDistanceCheck = .01f;
		public float moveSpeedMultiplier = 1f;
		public float scaleSpeedMultiplier = 1f;
		public float rotateSpeedMultiplier = 1f;
		public float allRotateSpeedMultiplier = 20f;

		#endregion Visual Settings

		public bool useFirstSelectedAsMain = true;

		/// <summary>
		/// If circularRotationMethod is true, when rotating you will need to move your mouse around the object as if turning a wheel.
		/// If circularRotationMethod is false, when rotating you can just click and drag in a line to rotate.
		/// </summary>
		public bool circularRotationMethod;

		/// <summary>
		/// Mainly for if you want the pivot point to update correctly if selected objects are moving outside the TransformGizmo.
		/// Might be poor on performance if lots of objects are selected...
		/// </summary>
		public bool forceUpdatePivotPointOnChange = true;

		public int maxUndoStored = 100;

		public bool manuallyHandleGizmo;

		/// <summary>
		/// The mask for selecting Objects with.
		/// </summary>
		public LayerMask selectionMask = Physics.DefaultRaycastLayers;

		public Action onCheckForSelectedAxis;
		public Action onDrawCustomGizmo;

		public Camera myCamera {get; private set;}

		public bool mouseIsOverGizmo => nearAxis != Axis.None;
		public bool isTransforming {get; private set;}
		public float totalScaleAmount {get; private set;}
		public Quaternion totalRotationAmount {get; private set;}

		/// <inheritdoc cref="nearAxis"/>
		public Axis translatingAxis => nearAxis;

		/// <inheritdoc cref="planeAxis"/>
		public Axis translatingAxisPlane => planeAxis;
		public bool hasTranslatingAxisPlane => translatingAxisPlane != Axis.None && translatingAxisPlane != Axis.Combined;
		public TransformType transformingType => translatingType;

		public Vector3 pivotPoint {get; private set;}
		private Vector3 totalCenterPivotPoint;

		public Transform mainTargetRoot => (targetRootsOrdered.Count > 0) ? (useFirstSelectedAsMain) ? targetRootsOrdered[0] : targetRootsOrdered[targetRootsOrdered.Count - 1] : null;

		public IReadOnlyCollection<Transform> targets => new ReadOnlyCollection<Transform>(targetRoots.Keys.ToList());

		#endregion Public fields

		#region Private fields

		private AxisInfo axisInfo;

		/// <summary>
		/// The axis the mouse is currently on causing it to be enabled and highlighted.
		/// </summary>
		private Axis nearAxis  = Axis.None;
		/// <summary>
		/// The Plane (two axis simultaneously) the mouse is currently on causing it to be enabled and highlighted.
		/// </summary>
		private Axis planeAxis = Axis.None;
		private TransformType translatingType;

		private Axis allowedMovementAxis = Axis.All;
		private Axis allowedRotationAxis = Axis.All;
		private Axis allowedScaleAxis = Axis.All;
		private bool allowRaycastMove = true;
		public LayerMask movementRayCastMask = Physics.DefaultRaycastLayers;

		private AxisVectors handleLines     = new AxisVectors();
		private AxisVectors handlePlanes    = new AxisVectors();
		private AxisVectors handleTriangles = new AxisVectors();
		private AxisVectors handleSquares   = new AxisVectors();
		private AxisVectors circlesLines    = new AxisVectors();

		// We use a HashSet and a List for targetRoots so that we get fast lookup with the hashset while also keeping track of the order with the list.
		/// <summary>
		/// Ordered list of targets
		/// </summary>
		private List<Transform> targetRootsOrdered = new List<Transform>();

		/// <summary>
		/// Dictionary of targets
		/// </summary>
		private Dictionary<Transform, TargetInfo> targetRoots = new Dictionary<Transform, TargetInfo>();

		/// <summary>
		/// The children (if any) of our targets.
		/// </summary>
		private HashSet<Transform> children = new HashSet<Transform>();

		private List<Transform> childrenBuffer = new List<Transform>();

		private WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
		private Coroutine forceUpdatePivotCoroutine;

		private static Material lineMaterial;

		#endregion Private fields

		#region UnityMessages

		private void Awake()
		{
			myCamera = GetComponent<Camera>();
			SetMaterial();
		}

		private void OnEnable()
		{
			forceUpdatePivotCoroutine = StartCoroutine(ForceUpdatePivotPointAtEndOfFrame());
		}

		private void OnDisable()
		{
			ClearTargets(); // Just so things gets cleaned up, such as removing any materials we placed on objects.

			StopCoroutine(forceUpdatePivotCoroutine);
		}

		private void Update()
		{
			if (null == AllowKeyboardInteraction || AllowKeyboardInteraction())
			{
				HandleUndoRedo();
				SetSpaceAndType();
			}

			if(manuallyHandleGizmo)
			{
				onCheckForSelectedAxis?.Invoke();
			}
			else
			{
				SetNearAxis();
			}

			if(null != CustomGetTarget)
			{
				CustomGetTarget();
			}
			else
			{
				GetTarget();
			}

			if(mainTargetRoot == null)
				return;

			TransformSelected();
		}

		private void LateUpdate()
		{
			if(mainTargetRoot == null)
				return;

			// We run this in LateUpdate since Coroutines run after update and 
			// we want our gizmos to have the updated target transform position after TransformSelected()
			SetAxisInfo();

			if(manuallyHandleGizmo)
			{
				onDrawCustomGizmo?.Invoke();
			}
			else
			{
				SetLines();
			}
		}

		private void OnPostRender()
		{
			if(mainTargetRoot == null || manuallyHandleGizmo)
				return;

			lineMaterial.SetPass(0);

			Color xColor = (nearAxis == Axis.X) ? (isTransforming) ? selectedColor : hoverColor : this.xColor;
			Color yColor = (nearAxis == Axis.Y) ? (isTransforming) ? selectedColor : hoverColor : this.yColor;
			Color zColor = (nearAxis == Axis.Z) ? (isTransforming) ? selectedColor : hoverColor : this.zColor;
			Color allColor =
				(nearAxis == Axis.Combined || nearAxis == Axis.Uniform) ? (isTransforming) ? selectedColor : hoverColor : this.allColor;

			// Note: The order of drawing the axis decides what gets drawn over what.

			TransformType moveOrScaleType = (transformType == TransformType.Scale || (isTransforming && translatingType == TransformType.Scale)) ? TransformType.Scale : TransformType.Move;
			DrawQuads(handleLines.z, GetColor(moveOrScaleType, this.zColor, zColor, hasTranslatingAxisPlane));
			DrawQuads(handleLines.x, GetColor(moveOrScaleType, this.xColor, xColor, hasTranslatingAxisPlane));
			DrawQuads(handleLines.y, GetColor(moveOrScaleType, this.yColor, yColor, hasTranslatingAxisPlane));

			DrawTriangles(handleTriangles.x, GetColor(TransformType.Move, this.xColor, xColor, hasTranslatingAxisPlane));
			DrawTriangles(handleTriangles.y, GetColor(TransformType.Move, this.yColor, yColor, hasTranslatingAxisPlane));
			DrawTriangles(handleTriangles.z, GetColor(TransformType.Move, this.zColor, zColor, hasTranslatingAxisPlane));

			DrawQuads(handlePlanes.z, GetColor(TransformType.Move, this.zColor, zColor, planesOpacity, !hasTranslatingAxisPlane));
			DrawQuads(handlePlanes.x, GetColor(TransformType.Move, this.xColor, xColor, planesOpacity, !hasTranslatingAxisPlane));
			DrawQuads(handlePlanes.y, GetColor(TransformType.Move, this.yColor, yColor, planesOpacity, !hasTranslatingAxisPlane));

			DrawQuads(handleSquares.x, GetColor(TransformType.Scale, this.xColor, xColor));
			DrawQuads(handleSquares.y, GetColor(TransformType.Scale, this.yColor, yColor));
			DrawQuads(handleSquares.z, GetColor(TransformType.Scale, this.zColor, zColor));
			DrawQuads(handleSquares.all, GetColor(TransformType.Scale, this.allColor, allColor));

			DrawQuads(circlesLines.all, GetColor(TransformType.Rotate, this.allColor, allColor));
			DrawQuads(circlesLines.x, GetColor(TransformType.Rotate, this.xColor, xColor));
			DrawQuads(circlesLines.y, GetColor(TransformType.Rotate, this.yColor, yColor));
			DrawQuads(circlesLines.z, GetColor(TransformType.Rotate, this.zColor, zColor));
		}

		#endregion UnityMessages

		#region GetColor

		private Color GetColor(TransformType type, Color normalColor, Color nearColor, bool forceUseNormal = false)
		{
			return GetColor(type, normalColor, nearColor, false, 1, forceUseNormal);
		}

		private Color GetColor(TransformType type, Color normalColor, Color nearColor, float alpha, bool forceUseNormal = false)
		{
			return GetColor(type, normalColor, nearColor, true, alpha, forceUseNormal);
		}

		private Color GetColor(TransformType type, Color normalColor, Color nearColor, bool setAlpha, float alpha, bool forceUseNormal = false)
		{
			Color color;
			if(!forceUseNormal && TranslatingTypeContains(type, false))
			{
				color = nearColor;
			}
			else
			{
				color = normalColor;
			}

			if(setAlpha)
			{
				color.a = alpha;
			}

			return color;
		}

		#endregion GetColor

		private void HandleUndoRedo()
		{
			if (maxUndoStored != UndoRedoManager.maxUndoStored)
			{
				UndoRedoManager.maxUndoStored = maxUndoStored;
			}

			if(Input.GetKey(ActionKey))
			{
				if(Input.GetKeyDown(UndoAction))
				{
					UndoRedoManager.Undo();
				}
				else if(Input.GetKeyDown(RedoAction))
				{
					UndoRedoManager.Redo();
				}
			}
		}


		// We only support scaling in local space.
		public TransformSpace GetProperTransformSpace()
		{
			return transformType == TransformType.Scale ? TransformSpace.Local : space;
		}

		#region TransformTypeContains
		public bool TransformTypeContains(TransformType type)
		{
			return TransformTypeContains(transformType, type);
		}
		public bool TranslatingTypeContains(TransformType type, bool checkIsTransforming = true)
		{
			TransformType transType = !checkIsTransforming || isTransforming ? translatingType : transformType;
			return TransformTypeContains(transType, type);
		}
		public bool TransformTypeContains(TransformType mainType, TransformType type)
		{
			return ExtTransformType.TransformTypeContains(mainType, type, GetProperTransformSpace());
		}

		#endregion TransformTypeContains

		public float GetHandleLength(TransformType type, Axis axis = Axis.None, bool multiplyDistanceMultiplier = true)
		{
			float length = handleLength;
			if(transformType == TransformType.All)
			{
				if(type == TransformType.Move)   length *= allMoveHandleLengthMultiplier;
				if(type == TransformType.Rotate) length *= allRotateHandleLengthMultiplier;
				if(type == TransformType.Scale)  length *= allScaleHandleLengthMultiplier;
			}

			if(multiplyDistanceMultiplier) length *= GetDistanceMultiplier();

			if(type == TransformType.Scale
			&& isTransforming
			&& (translatingAxis == axis || translatingAxis == Axis.Combined))
				length += totalScaleAmount;

			return length;
		}

		private void SetSpaceAndType()
		{
			if(Input.GetKey(ActionKey)) return;

			if (Input.GetKeyDown(SetNoneType)) transformType = TransformType.None;
			else if (Input.GetKeyDown(SetMoveType) && allowedMovementAxis != Axis.None)
			{
				transformType = TransformType.Move;
			}
			else if (Input.GetKeyDown(SetRotateType) && allowedRotationAxis != Axis.None)
			{
				transformType = TransformType.Rotate;
			}
			else if (Input.GetKeyDown(SetScaleType) && allowedScaleAxis != Axis.None)
			{
				transformType = TransformType.Scale;
			}
			else if (Input.GetKeyDown(SetRayCastType) && allowRaycastMove)
			{
				transformType = TransformType.RayCast;
			}
			else if (Input.GetKeyDown(SetAllTransformType))
			{
				transformType = TransformType.All;
			}

			if(!isTransforming) translatingType = transformType;


			if (pivot == TransformPivot.None || ! EnumValidator<TransformPivot>.IsDefined(pivot))
			{
				pivot = TransformPivot.Origin;
			}

			if(Input.GetKeyDown(SetPivotModeToggle))
			{
				if(pivot == TransformPivot.Origin) pivot = TransformPivot.Center;
				else if(pivot == TransformPivot.Center) pivot = TransformPivot.Origin;

				SetPivotPoint();
			}

			if(Input.GetKeyDown(SetCenterTypeToggle))
			{
				if(centerType == CenterType.All) centerType = CenterType.Solo;
				else if(centerType == CenterType.Solo) centerType = CenterType.All;

				SetPivotPoint();
			}

			if(Input.GetKeyDown(SetSpaceToggle))
			{
				if(space == TransformSpace.Global) space = TransformSpace.Local;
				else if(space == TransformSpace.Local) space = TransformSpace.Global;
			}

			if(Input.GetKeyDown(SetScaleTypeToggle))
			{
				if(scaleType == ScaleType.FromPoint) scaleType = ScaleType.FromPointOffset;
				else if(scaleType == ScaleType.FromPointOffset) scaleType = ScaleType.FromPoint;
			}

			if(transformType == TransformType.Scale)
			{
				// FromPointOffset can be inaccurate and should only really be used in Center mode if desired.
				if (pivot == TransformPivot.Origin) scaleType = ScaleType.FromPoint;
			}
		}

		/// <summary>
		/// Start the RayCastDrag transform mode immediately (bypassing the normal control inputs.
		/// Note that the proper mouse buttons should be down, otherwise the operation will stop immediately.
		/// </summary>
		public void ForceStartRaycastDrag()
		{
			if (mainTargetRoot == null)
			{
				throw new NullReferenceException($"There is no target Assigned.");
			}

			if (! allowRaycastMove)
			{
				// todo: should we even check?
				throw new Exception($"{nameof(TransformType.RayCast)} is not allowed for the current selection.");
			}

			transformType = TransformType.RayCast;

			StartCoroutine(TransformSelected_RayCastDrag());
		}

		private void TransformSelected()
		{
			if(mainTargetRoot != null && Input.GetMouseButtonDown(0))
			{
				if (transformType == TransformType.RayCast
				 && allowRaycastMove) // TransformType could be set externally while it's not allowed.
				{
					StartCoroutine(TransformSelected_RayCastDrag());
				}
				else if(nearAxis != Axis.None)
				{
					StartCoroutine(TransformSelected(translatingType));
				}
			}
		}

		private IEnumerator TransformSelected(TransformType transType)
		{
			isTransforming = true;
			totalScaleAmount = 0;
			totalRotationAmount = Quaternion.identity;

			Vector3 originalPivot = pivotPoint;

			Vector3 otherAxis1, otherAxis2;
			Vector3 axis = GetNearAxisDirection(out otherAxis1, out otherAxis2);
			Vector3 planeNormal = hasTranslatingAxisPlane ? axis : (transform.position - originalPivot).normalized;
			Vector3 projectedAxis = Vector3.ProjectOnPlane(axis, planeNormal).normalized;
			Vector3 previousMousePosition = Vector3.zero;

			Vector3 currentSnapMovementAmount = Vector3.zero;
			float currentSnapRotationAmount = 0;
			float currentSnapScaleAmount = 0;

			List<ICommand> transformCommands = new List<ICommand>(targetRootsOrdered.Count);
			foreach (Transform t in targetRootsOrdered)
			{
				transformCommands.Add(new TransformCommand(this, t));

				var b = t.GetComponent<TransformGizmoEventReceiver>();
				if (null != b)
					b.FireGizmoStartInteract();
			}

			while(!Input.GetMouseButtonUp(0))
			{
				Ray mouseRay = myCamera.ScreenPointToRay(Input.mousePosition);
				Vector3 mousePosition = Geometry.LinePlaneIntersect(
					mouseRay.origin,
					mouseRay.direction,
					originalPivot,
					planeNormal);
				bool isSnapping = Input.GetKey(translationSnapping);

				if(previousMousePosition != Vector3.zero && mousePosition != Vector3.zero)
				{
					// todo?: the Type cannot change while interacting, so each could be it's own iterator.
					if(transType == TransformType.Move)
					{
						TransformSelected_Move(
							mousePosition,
							previousMousePosition,
							projectedAxis,
							axis,
							isSnapping,
							ref currentSnapMovementAmount,
							otherAxis1,
							otherAxis2);
					}
					else if(transType == TransformType.Scale)
					{
						TransformSelected_Scale(projectedAxis,
							mousePosition,
							previousMousePosition,
							isSnapping,
							ref currentSnapScaleAmount,
							axis,
							originalPivot);
					}
					else if(transType == TransformType.Rotate)
					{
						TransformSelected_Rotate(axis,
							previousMousePosition,
							originalPivot,
							mousePosition,
							planeNormal,
							isSnapping,
							ref currentSnapRotationAmount);
					}
					else
					{
						throw new Exception($"Unknown TransformType '{transType}'.");
					}

					foreach (Transform t in targetRootsOrdered)
					{
						var b = t.GetComponent<TransformGizmoEventReceiver>();
						if(null != b)
							b.FireGizmoInteract();
					}
				}

				previousMousePosition = mousePosition;

				yield return null;
			}

			foreach (ICommand t in transformCommands)
			{
				((TransformCommand)t).StoreNewTransformValues();
			}
			CommandGroup commandGroup = new CommandGroup();
			commandGroup.Set(transformCommands);
			UndoRedoManager.Insert(commandGroup);

			totalRotationAmount = Quaternion.identity;
			totalScaleAmount = 0;
			isTransforming = false;
			SetTranslatingAxis(transformType, Axis.None);

			SetPivotPoint();

			foreach (Transform t in targetRootsOrdered)
			{
				var b = t.GetComponent<TransformGizmoEventReceiver>();
				if(b != null)
					b.FireGizmoEndInteract();
			}
		}


		private void TransformSelected_Move
		(
			Vector3 mousePosition
		  , Vector3 previousMousePosition
		  , Vector3 projectedAxis
		  , Vector3 axis
		  , bool isSnapping
		  , ref Vector3 currentSnapMovementAmount
		  , Vector3 otherAxis1
		  , Vector3 otherAxis2
		)
		{
			Vector3 movement;

			if (hasTranslatingAxisPlane)
			{
				movement = mousePosition - previousMousePosition;
			}
			else
			{
				float moveAmount = ExtVector3.MagnitudeInDirection
									   (mousePosition - previousMousePosition, projectedAxis)
								 * moveSpeedMultiplier;
				movement = axis * moveAmount;
			}

			if (isSnapping && movementSnap > 0)
			{
				currentSnapMovementAmount += movement;
				movement = Vector3.zero;

				if (hasTranslatingAxisPlane)
				{
					float amountInAxis1 = ExtVector3.MagnitudeInDirection(
						currentSnapMovementAmount, otherAxis1);
					float amountInAxis2 = ExtVector3.MagnitudeInDirection(
						currentSnapMovementAmount, otherAxis2);

					float remainder1;
					float snapAmount1 = CalculateSnapAmount(movementSnap, amountInAxis1, out remainder1);
					float remainder2;
					float snapAmount2 = CalculateSnapAmount(movementSnap, amountInAxis2, out remainder2);

					if (snapAmount1 != 0)
					{
						Vector3 snapMove = (otherAxis1 * snapAmount1);
						movement += snapMove;
						currentSnapMovementAmount -= snapMove;
					}

					if (snapAmount2 != 0)
					{
						Vector3 snapMove = (otherAxis2 * snapAmount2);
						movement += snapMove;
						currentSnapMovementAmount -= snapMove;
					}
				}
				else
				{
					float snapAmount = CalculateSnapAmount
						(movementSnap, currentSnapMovementAmount.magnitude, out float remainder);

					if (snapAmount != 0)
					{
						movement = currentSnapMovementAmount.normalized * snapAmount;
						currentSnapMovementAmount = currentSnapMovementAmount.normalized * remainder;
					}
				}
			}

			foreach (Transform target in targetRootsOrdered)
			{
				target.Translate(movement, Space.World);
			}

			SetPivotPointOffset(movement);
		}


		private void TransformSelected_Scale
		(
			Vector3   projectedAxis
		  , Vector3   mousePosition
		  , Vector3   previousMousePosition
		  , bool      isSnapping
		  , ref float currentSnapScaleAmount
		  , Vector3   axis
		  , Vector3   originalPivot
		)
		{
			Vector3 projected = (nearAxis == Axis.Uniform) ? transform.right : projectedAxis;
			float scaleAmount = ExtVector3.MagnitudeInDirection(
				mousePosition - previousMousePosition, projected
				) * scaleSpeedMultiplier;

			if (isSnapping && scaleSnap > 0)
			{
				currentSnapScaleAmount += scaleAmount;
				scaleAmount = 0;

				float snapAmount = CalculateSnapAmount(scaleSnap, currentSnapScaleAmount, out float remainder);

				if (snapAmount != 0)
				{
					scaleAmount = snapAmount;
					currentSnapScaleAmount = remainder;
				}
			}

			// WARNING - There is a bug in unity 5.4 and 5.5 that causes InverseTransformDirection to be affected by scale which will break negative scaling. Not tested, but updating to 5.4.2 should fix it - https://issuetracker.unity3d.com/issues/transformdirection-and-inversetransformdirection-operations-are-affected-by-scale
			Vector3 localAxis = (GetProperTransformSpace() == TransformSpace.Local && nearAxis != Axis.Uniform)
				? mainTargetRoot.InverseTransformDirection(axis)
				: axis;

			Vector3 targetScaleAmount;
			if (nearAxis == Axis.Uniform)
				targetScaleAmount = (ExtVector3.Abs(mainTargetRoot.localScale.normalized) * scaleAmount);
			else
				targetScaleAmount = localAxis * scaleAmount;

			foreach (Transform target in targetRootsOrdered)
			{
				Vector3 targetScale = target.localScale + targetScaleAmount;

				if (pivot == TransformPivot.Origin)
				{
					target.localScale = targetScale;
				}
				else if (pivot == TransformPivot.Center)
				{
					if (scaleType == ScaleType.FromPoint)
					{
						target.SetScaleFrom(originalPivot, targetScale);
					}
					else if (scaleType == ScaleType.FromPointOffset)
					{
						target.SetScaleFromOffset(originalPivot, targetScale);
					}
				}
			}

			totalScaleAmount += scaleAmount;
		}



		private void TransformSelected_Rotate
		(
			Vector3 axis
		  , Vector3 previousMousePosition
		  , Vector3 originalPivot
		  , Vector3 mousePosition
		  , Vector3 planeNormal
		  , bool isSnapping
		  , ref float currentSnapRotationAmount
		)
		{
			float rotateAmount;
			Vector3 rotationAxis = axis;

			if (nearAxis == Axis.Combined)
			{
				Vector3 rotation = transform.TransformDirection
					(new Vector3(Input.GetAxis("Mouse Y"), -Input.GetAxis("Mouse X"), 0));
				Quaternion.Euler(rotation).ToAngleAxis(out rotateAmount, out rotationAxis);
				rotateAmount *= allRotateSpeedMultiplier;
			}
			else
			{
				if (circularRotationMethod)
				{
					float angle = Vector3.SignedAngle
						(previousMousePosition - originalPivot, mousePosition - originalPivot, axis);
					rotateAmount = angle * rotateSpeedMultiplier;
				}
				else
				{
					Vector3 projected = (nearAxis == Axis.Combined || ExtVector3.IsParallel(axis, planeNormal))
						? planeNormal
						: Vector3.Cross(axis, planeNormal);
					rotateAmount = (ExtVector3.MagnitudeInDirection
										(mousePosition - previousMousePosition, projected)
								  * (rotateSpeedMultiplier * 100f))
								 / GetDistanceMultiplier();
				}
			}

			if (isSnapping && rotationSnap > 0)
			{
				currentSnapRotationAmount += rotateAmount;
				rotateAmount = 0;

				float snapAmount = CalculateSnapAmount(rotationSnap, currentSnapRotationAmount, out float remainder);

				if (snapAmount != 0)
				{
					rotateAmount = snapAmount;
					currentSnapRotationAmount = remainder;
				}
			}

			foreach (Transform target in targetRootsOrdered)
			{
				if (pivot == TransformPivot.Origin)
				{
					target.Rotate(rotationAxis, rotateAmount, Space.World);
				}
				else if (pivot == TransformPivot.Center)
				{
					target.RotateAround(originalPivot, rotationAxis, rotateAmount);
				}
			}

			totalRotationAmount *= Quaternion.Euler(rotationAxis * rotateAmount);
		}


		private IEnumerator TransformSelected_RayCastDrag()
		{
			LayerMask mask = movementRayCastMask;
			var triggerInteraction = QueryTriggerInteraction.Ignore;

			// Initialization -------------------------------------------------
			Vector3 lastHitPoint;
			{
				Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);

				if (TryMyCustomDragRayCast(ray, mask, triggerInteraction, out RaycastHit hit))
				{
					lastHitPoint = hit.point;
				}
				else
				{
					// no hit, abort.
					// todo?: Complain?
					yield break;
				}
			}

			List<ICommand> transformCommands = new List<ICommand>();
			foreach (Transform t in targetRootsOrdered)
			{
				transformCommands.Add(new TransformCommand(this, t));

				var b = t.GetComponent<TransformGizmoEventReceiver>();
				if (null != b)
					b.FireGizmoStartInteract();
			}


			// Dragging -------------------------------------------------------
			isTransforming = true;

			while (! Input.GetMouseButtonUp(0))
			{
				Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);

				if (TryMyCustomDragRayCast(ray, mask, triggerInteraction, out RaycastHit hit))
				{
					Vector3 dragDistance = lastHitPoint - hit.point;

					for (int i = 0; i < targetRootsOrdered.Count; i++)
					{
						Transform target = targetRootsOrdered[i];

						target.Translate(-dragDistance, Space.World);
					}

					lastHitPoint = hit.point;
				}
				else
				{
					// todo: should we stop?
				}

				foreach (Transform t in targetRootsOrdered)
				{
					var b = t.GetComponent<TransformGizmoEventReceiver>();
					if (null != b)
						b.FireGizmoInteract();
				}
				yield return null;
			}

			// End ------------------------------------------------------------
			foreach (Transform t in targetRootsOrdered)
			{
				var b = t.GetComponent<TransformGizmoEventReceiver>();
				if (null != b)
					b.FireGizmoEndInteract();
			}
			foreach (ICommand t in transformCommands)
			{
				((TransformCommand)t).StoreNewTransformValues();
			}

			CommandGroup commandGroup = new CommandGroup();
			commandGroup.Set(transformCommands);
			UndoRedoManager.Insert(commandGroup);

			isTransforming = false;
		}

		/// <summary>
		/// Returns all RayCast hits, in ascending order of distance.
		/// </summary>
		/// <param name="ray"></param>
		/// <param name="mask"></param>
		/// <param name="triggerInteraction"></param>
		/// <returns></returns>
		private RaycastHit[] MyCustomRayCastMany(in Ray ray, LayerMask mask, QueryTriggerInteraction triggerInteraction)
		{
			var hits = Physics.RaycastAll(ray, float.MaxValue, mask, triggerInteraction);

			Array.Sort(hits, (a, b) => a.distance.CompareTo(b.distance));

			return hits;
		}

		/// <summary>
		/// Attempts to get a <see cref="RaycastHit"/> that does not hit any of the selected targets, or their children.
		/// </summary>
		/// <param name="ray">The <see cref="Ray"/> to cast.</param>
		/// <param name="mask">The <see cref="LayerMask"/> to find collision with.</param>
		/// <param name="triggerInteraction"></param>
		/// <param name="hit">The resulting hit, or default if returned false.</param>
		/// <returns><see langword="true"/> if found, <see langword="false"/> if not.</returns>
		private bool TryMyCustomDragRayCast(
			in Ray ray,
			LayerMask mask,
			QueryTriggerInteraction triggerInteraction,
			out RaycastHit hit)
		{
			hit = default;

			bool hasHit = false;
			RaycastHit[] hits = MyCustomRayCastMany(ray, mask, triggerInteraction);
			foreach (var potentialHit in hits)
			{
				var hitTransform = potentialHit.transform;
				if (! targetRoots.ContainsKey(hitTransform)
				 && ! children.Contains(hitTransform))
				{
					hit = potentialHit;
					hasHit = true;
					break;
				}
			}

			return hasHit;
		}

		private float CalculateSnapAmount(float snapValue, float currentAmount, out float remainder)
		{
			remainder = 0;
			if(snapValue <= 0) return currentAmount;

			float currentAmountAbs = Mathf.Abs(currentAmount);
			if(currentAmountAbs > snapValue)
			{
				remainder = currentAmountAbs % snapValue;
				return snapValue * (Mathf.Sign(currentAmount) * Mathf.Floor(currentAmountAbs / snapValue));
			}

			return 0;
		}

		private Vector3 GetNearAxisDirection(out Vector3 otherAxis1, out Vector3 otherAxis2)
		{
			otherAxis1 = otherAxis2 = Vector3.zero;

			if(nearAxis != Axis.None)
			{
				if(nearAxis == Axis.X)
				{
					otherAxis1 = axisInfo.yDirection;
					otherAxis2 = axisInfo.zDirection;
					return axisInfo.xDirection;
				}
				if(nearAxis == Axis.Y)
				{
					otherAxis1 = axisInfo.xDirection;
					otherAxis2 = axisInfo.zDirection;
					return axisInfo.yDirection;
				}
				if(nearAxis == Axis.Z)
				{
					otherAxis1 = axisInfo.xDirection;
					otherAxis2 = axisInfo.yDirection;
					return axisInfo.zDirection;
				}
				if(nearAxis == Axis.Combined)
				{
					return Vector3.one;
				}
			}

			return Vector3.zero;
		}


		#region Target management

		/// <summary>
		/// Restrict the allowed transform handles based on the options set in the provided <see cref="TransformGizmoEventReceiver"/>.
		/// </summary>
		/// <param name="options"></param>
		private void RestrictAllowedAxisAndPivot(TransformGizmoEventReceiver options)
		{
			allowedMovementAxis &= options.AllowedMovementAxis;
			allowedRotationAxis &= options.AllowedRotateAxis;
			allowedScaleAxis    &= options.AllowedScaleAxis;
			allowRaycastMove    &= options.AllowRaycastMove;
			movementRayCastMask &= options.RayCastLayerMask;
			pivot &= options.AllowedPivot;

			if (! EnumValidator<TransformPivot>.IsDefined(pivot)) pivot = TransformPivot.Origin;

			// Ensure that a disallowed transformType is disabled when it should
			if (transformType == TransformType.Move && allowedMovementAxis == Axis.None)
				transformType = TransformType.None;

			if (transformType == TransformType.Scale && allowedScaleAxis == Axis.None)
				transformType = TransformType.None;

			if (transformType == TransformType.Rotate && allowedRotationAxis == Axis.None)
				transformType = TransformType.None;

			if (transformType == TransformType.RayCast && ! allowRaycastMove)
				transformType = TransformType.None;
		}

		/// <summary>
		/// Evaluate all targets to see what <see cref="TransformType"/>s and <see cref="Axis"/> are allowed to be used.
		/// </summary>
		private void EvaluateAllowedAxis()
		{
			allowedMovementAxis = Axis.All;
			allowedRotationAxis = Axis.All;
			allowedScaleAxis    = Axis.All;
			allowRaycastMove = true;
			movementRayCastMask = Physics.DefaultRaycastLayers;


			foreach (Transform t in targetRootsOrdered)
			{
				var gizmoEvents = t.GetComponent<TransformGizmoEventReceiver>();

				if(null == gizmoEvents) continue;

				RestrictAllowedAxisAndPivot(gizmoEvents);
			}
		}

		/// <summary>
		/// Test if the combination <paramref name="type"/> and <paramref name="axis"/> is allowed to be used.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="axis"></param>
		/// <returns></returns>
		private bool IsAllowedAxis(TransformType type, Axis axis)
		{
			switch (type)
			{
				case TransformType.Move:
				{
					return (allowedMovementAxis & axis) == axis;
				}
				case TransformType.Rotate:
				{
					return (allowedRotationAxis & axis) == axis;
				}
				case TransformType.Scale:
				{
					return (allowedScaleAxis & axis) == axis;
				}
				case TransformType.RayCast:
				{
					return allowRaycastMove;
				}
				case TransformType.None:
				{
					return axis == Axis.None;
				}
				case TransformType.All:
				{
					return IsAllowedAxis(TransformType.Move,   axis)
					    && IsAllowedAxis(TransformType.Rotate, axis)
					    && IsAllowedAxis(TransformType.Scale,  axis);
				}
				default: throw new Exception($"Unexpected {nameof(TransformType)} '{type}'");
			}
		}

		/// <summary>
		/// Get a target from user input.
		/// </summary>
		private void GetTarget()
		{
			if(nearAxis == Axis.None && Input.GetMouseButtonDown(0))
			{
				bool isAdding = Input.GetKey(AddSelection);
				bool isRemoving = Input.GetKey(RemoveSelection);

				if(Physics.Raycast(myCamera.ScreenPointToRay(Input.mousePosition),
				                   out RaycastHit hitInfo,
				                   Mathf.Infinity,
				                   selectionMask))
				{
					Transform target = hitInfo.transform;

					if (isAdding)
					{
						AddTarget(target);
					}
					else if (isRemoving)
					{
						RemoveTarget(target);
					}
					else if (! isAdding && ! isRemoving)
					{
						ClearAndAddTarget(target);
					}
				}
				else
				{
					OnRayMiss();
				}

				void OnRayMiss()
				{
					if (!isAdding && !isRemoving)
					{
						bool raiseEvent = targets.Count > 0;
						ClearTargets();
					}
				}
			}
		}

		/// <summary>
		/// Add a target.
		/// </summary>
		/// <param name="target"></param>
		/// <param name="addCommand"></param>
		public void AddTarget(Transform target, bool addCommand = true)
		{
			if(target != null)
			{
				if(targetRoots.ContainsKey(target)) return;
				if(children.Contains(target)) return;

				if(addCommand) UndoRedoManager.Insert(new AddTargetCommand(this, target, targetRootsOrdered));

				AddTargetRoot(target);

				SetPivotPoint();

				OnSelectionChanged?.Invoke();

				var gizmoEvents = target.GetComponent<TransformGizmoEventReceiver>();
				if (null != gizmoEvents)
				{
					RestrictAllowedAxisAndPivot(gizmoEvents);

					gizmoEvents.FireGizmoAttach();
				}
			}
		}

		public void RemoveTarget(Transform target, bool addCommand = true)
		{
			if(target != null)
			{
				if(!targetRoots.ContainsKey(target)) return;

				if(addCommand) UndoRedoManager.Insert(new RemoveTargetCommand(this, target));

				RemoveTargetRoot(target);

				SetPivotPoint();

				OnSelectionChanged?.Invoke();

				// todo: children?
				var gizmoEvents = target.GetComponent<TransformGizmoEventReceiver>();
				if (null != gizmoEvents)
				{
					// If there was a GizmoEvents then removing it could cause
					// the allowed axis to change.
					EvaluateAllowedAxis();

					gizmoEvents.FireGizmoDetach();
				}
			}
		}

		public void ClearTargets(bool addCommand = true)
		{
			ClearTargetsInternal(addCommand, targetRootsOrdered.Count > 0);
		}

		private void ClearTargetsInternal(bool addCommand = true, bool raiseEvent = true)
		{
			if (addCommand) UndoRedoManager.Insert(new ClearTargetsCommand(this, targetRootsOrdered));

			foreach (Transform t in targetRootsOrdered)
			{
				// todo: children?

				// todo: this is a hack: it shouldn't ever be null!
				if (null != t)
				{
					var b = t.GetComponent<TransformGizmoEventReceiver>();
					if (null != b)
						b.FireGizmoDetach();
				}
			}
			targetRoots.Clear();
			targetRootsOrdered.Clear();
			children.Clear();

			if (raiseEvent) OnSelectionChanged?.Invoke();

			EvaluateAllowedAxis();
		}

		public void ClearAndAddTarget(Transform target)
		{
			UndoRedoManager.Insert(new ClearAndAddTargetCommand(this, target, targetRootsOrdered));

			ClearTargetsInternal(false, false);
			AddTarget(target, false);
		}


		private void AddTargetRoot(Transform targetRoot)
		{
			targetRoots.Add(targetRoot, new TargetInfo());
			targetRootsOrdered.Add(targetRoot);

			AddAllChildren(targetRoot);
		}

		private void RemoveTargetRoot(Transform targetRoot)
		{
			if(targetRoots.Remove(targetRoot))
			{
				targetRootsOrdered.Remove(targetRoot);

				RemoveAllChildren(targetRoot);
			}
		}

		private void AddAllChildren(Transform target)
		{
			childrenBuffer.Clear();
			target.GetComponentsInChildren<Transform>(true, childrenBuffer);
			childrenBuffer.Remove(target);

			for(int i = 0; i < childrenBuffer.Count; i++)
			{
				Transform child = childrenBuffer[i];
				children.Add(child);
				RemoveTargetRoot(child); // We do this in case we selected child first and then the parent.
			}

			childrenBuffer.Clear();
		}

		private void RemoveAllChildren(Transform target)
		{
			childrenBuffer.Clear();
			target.GetComponentsInChildren<Transform>(true, childrenBuffer);
			childrenBuffer.Remove(target);

			for(int i = 0; i < childrenBuffer.Count; i++)
			{
				children.Remove(childrenBuffer[i]);
			}

			childrenBuffer.Clear();
		}

		#endregion Target management

		#region Pivot

		public void SetPivotPoint()
		{
			if(mainTargetRoot != null)
			{
				if(pivot == TransformPivot.Origin)
				{
					pivotPoint = mainTargetRoot.position;
				}
				else if(pivot == TransformPivot.Center)
				{
					totalCenterPivotPoint = Vector3.zero;

					// We avoid foreach to avoid garbage with the explicit Enumerator.
					Dictionary<Transform, TargetInfo>.Enumerator targetsEnumerator = targetRoots.GetEnumerator();
					while(targetsEnumerator.MoveNext())
					{
						Transform target = targetsEnumerator.Current.Key;
						TargetInfo info = targetsEnumerator.Current.Value;
						info.centerPivotPoint = target.GetCenter(centerType);

						totalCenterPivotPoint += info.centerPivotPoint;
					}

					totalCenterPivotPoint /= targetRoots.Count;

					if(centerType == CenterType.Solo)
					{
						pivotPoint = targetRoots[mainTargetRoot].centerPivotPoint;
					}
					else if(centerType == CenterType.All)
					{
						pivotPoint = totalCenterPivotPoint;
					}
				}
			}
		}

		private void SetPivotPointOffset(Vector3 offset)
		{
			pivotPoint += offset;
			totalCenterPivotPoint += offset;
		}


		private IEnumerator ForceUpdatePivotPointAtEndOfFrame()
		{
			while(this.enabled)
			{
				ForceUpdatePivotPointOnChange();
				yield return waitForEndOfFrame;
			}
		}

		private void ForceUpdatePivotPointOnChange()
		{
			if(forceUpdatePivotPointOnChange)
			{
				if(mainTargetRoot != null && !isTransforming)
				{
					bool hasSet = false;
					Dictionary<Transform, TargetInfo>.Enumerator targets = targetRoots.GetEnumerator();
					while(targets.MoveNext())
					{
						if(!hasSet)
						{
							if(targets.Current.Value.previousPosition != Vector3.zero && targets.Current.Key.position != targets.Current.Value.previousPosition)
							{
								SetPivotPoint();
								hasSet = true;
							}
						}

						targets.Current.Value.previousPosition = targets.Current.Key.position;
					}
				}
			}
		}

		#endregion Pivot

		#region Gizmo Interaction

		public void SetTranslatingAxis(TransformType type, Axis axis)
		{
			if (! IsAllowedAxis(type, axis)) return;

			this.translatingType = type;
			this.nearAxis = axis;
			this.planeAxis = Axis.None;
		}

		public void SetTranslatingPlane(TransformType type, Axis planeAxis)
		{
			// The PlaneAxis is whatever two axis the plane isn't on.
			// This confuses the "Is this axis allowed" logic.
			// Because it only cares about the axis of movement, not the mathematical designation of the plane.
			// Thus it needs to be converted.
			// Todo: refactor this logic to function

			Axis checkAxis;

			switch (planeAxis)
			{
				case Axis.X:
				{
					checkAxis = Axis.Y | Axis.Z;
					break;
				}
				case Axis.Y:
				{
					checkAxis = Axis.X | Axis.Z;
					break;
				}
				case Axis.Z:
				{
					checkAxis = Axis.X | Axis.Y;
					break;
				}
				default:
				{
					checkAxis = Axis.None;
					break;
				}
			}

			if (!IsAllowedAxis(type, checkAxis)) return;

			this.translatingType = type;

			// Yes, this seems wrong, but that's how the rest of the code works.
			this.nearAxis = planeAxis;
			this.planeAxis = Axis.None;
		}

		public AxisInfo GetAxisInfo()
		{
			AxisInfo currentAxisInfo = axisInfo;

			if(isTransforming && GetProperTransformSpace() == TransformSpace.Global && translatingType == TransformType.Rotate)
			{
				currentAxisInfo.xDirection = totalRotationAmount * Vector3.right;
				currentAxisInfo.yDirection = totalRotationAmount * Vector3.up;
				currentAxisInfo.zDirection = totalRotationAmount * Vector3.forward;
			}

			return currentAxisInfo;
		}

		private void SetNearAxis()
		{
			if(isTransforming) return;

			SetTranslatingAxis(transformType, Axis.None);

			if(mainTargetRoot == null) return;

			float distanceMultiplier = GetDistanceMultiplier();
			float handleMinSelectedDistanceCheck = (this.minSelectedDistanceCheck + handleWidth) * distanceMultiplier;

			if(nearAxis == Axis.None && (TransformTypeContains(TransformType.Move) || TransformTypeContains(TransformType.Scale)))
			{
				// Important to check scale lines before move lines since in TransformType.All the move planes would block the scales center scale all gizmo.
				if(nearAxis == Axis.None && TransformTypeContains(TransformType.Scale))
				{
					float tipMinSelectedDistanceCheck = (this.minSelectedDistanceCheck + boxSize) * distanceMultiplier;
					HandleNearestPlanes(TransformType.Scale, handleSquares, tipMinSelectedDistanceCheck);
				}

				if(nearAxis == Axis.None && TransformTypeContains(TransformType.Move))
				{
					// Important to check the planes first before the handle tip since it makes selecting the planes easier.
					float planeMinSelectedDistanceCheck = (this.minSelectedDistanceCheck + planeSize) * distanceMultiplier;
					HandleNearestPlanes(TransformType.Move, handlePlanes, planeMinSelectedDistanceCheck);

					if(nearAxis != Axis.None)
					{
						planeAxis = nearAxis;
					}
					else
					{
						float tipMinSelectedDistanceCheck = (this.minSelectedDistanceCheck + triangleSize) * distanceMultiplier;
						HandleNearestLines(TransformType.Move, handleTriangles, tipMinSelectedDistanceCheck);
					}
				}

				if(nearAxis == Axis.None)
				{
					// Since Move and Scale share the same handle line, we give Move the priority.
					TransformType transType = transformType == TransformType.All ? TransformType.Move : transformType;
					HandleNearestLines(transType, handleLines, handleMinSelectedDistanceCheck);
				}
			}

			if(nearAxis == Axis.None && TransformTypeContains(TransformType.Rotate))
			{
				HandleNearestLines(TransformType.Rotate, circlesLines, handleMinSelectedDistanceCheck);
			}
		}

		private void HandleNearestLines(TransformType type, AxisVectors axisVectors, float minSelectedDistanceCheck)
		{
			float xClosestDistance = ClosestDistanceFromMouseToLines(axisVectors.x);
			float yClosestDistance = ClosestDistanceFromMouseToLines(axisVectors.y);
			float zClosestDistance = ClosestDistanceFromMouseToLines(axisVectors.z);
			float allClosestDistance = ClosestDistanceFromMouseToLines(axisVectors.all);

			var newNearAxis = HandleNearest(type, xClosestDistance, yClosestDistance, zClosestDistance, allClosestDistance, minSelectedDistanceCheck);
			SetTranslatingAxis(type, newNearAxis);
		}

		private void HandleNearestPlanes(TransformType type, AxisVectors axisVectors, float minSelectedDistanceCheck)
		{
			float xClosestDistance = ClosestDistanceFromMouseToPlanes(axisVectors.x);
			float yClosestDistance = ClosestDistanceFromMouseToPlanes(axisVectors.y);
			float zClosestDistance = ClosestDistanceFromMouseToPlanes(axisVectors.z);
			float allClosestDistance = ClosestDistanceFromMouseToPlanes(axisVectors.all);

			var newNearAxis = HandleNearest(type, xClosestDistance, yClosestDistance, zClosestDistance, allClosestDistance, minSelectedDistanceCheck);
			SetTranslatingPlane(type, newNearAxis);
		}

		private Axis HandleNearest(TransformType type, float xClosestDistance, float yClosestDistance, float zClosestDistance, float allClosestDistance, float minSelectedDistanceCheck)
		{
			if (type == TransformType.Scale && allClosestDistance <= minSelectedDistanceCheck)
			{
				return Axis.Uniform;
			}
			else if (xClosestDistance <= minSelectedDistanceCheck
			      && xClosestDistance <= yClosestDistance
			      && xClosestDistance <= zClosestDistance)
			{
				return Axis.X;
			}
			else if (yClosestDistance <= minSelectedDistanceCheck
			      && yClosestDistance <= xClosestDistance
			      && yClosestDistance <= zClosestDistance)
			{
				return Axis.Y;
			}
			else if (zClosestDistance <= minSelectedDistanceCheck
			      && zClosestDistance <= xClosestDistance
			      && zClosestDistance <= yClosestDistance)
			{
				return Axis.Z;
			}
			else if(type == TransformType.Rotate && mainTargetRoot != null)
			{
				Ray mouseRay = myCamera.ScreenPointToRay(Input.mousePosition);
				Vector3 mousePlaneHit = Geometry.LinePlaneIntersect
				(
					mouseRay.origin,
					mouseRay.direction,
					pivotPoint,
					(transform.position - pivotPoint).normalized
				);
				float handleLength = GetHandleLength(TransformType.Rotate).Squared();
				if ((pivotPoint - mousePlaneHit).sqrMagnitude <= handleLength)
				{
					return Axis.Combined;
				}
			}

			return Axis.None;
		}

		private float ClosestDistanceFromMouseToLines(List<Vector3> lines)
		{
			Ray mouseRay = myCamera.ScreenPointToRay(Input.mousePosition);

			float closestDistance = float.MaxValue;
			for(int i = 0; i + 1 < lines.Count; i++)
			{
				IntersectPoints points = Geometry.ClosestPointsOnSegmentToLine(lines[i], lines[i + 1], mouseRay.origin, mouseRay.direction);
				float distance = Vector3.Distance(points.first, points.second);
				if(distance < closestDistance)
				{
					closestDistance = distance;
				}
			}
			return closestDistance;
		}

		private float ClosestDistanceFromMouseToPlanes(List<Vector3> planePoints)
		{
			float closestDistance = float.MaxValue;

			if(planePoints.Count >= 4)
			{
				Ray mouseRay = myCamera.ScreenPointToRay(Input.mousePosition);

				for(int i = 0; i < planePoints.Count; i += 4)
				{
					Plane plane = new Plane(planePoints[i], planePoints[i + 1], planePoints[i + 2]);

					if(plane.Raycast(mouseRay, out float distanceToPlane))
					{
						Vector3 pointOnPlane = mouseRay.origin + (mouseRay.direction * distanceToPlane);
						Vector3 planeCenter = (planePoints[0] + planePoints[1] + planePoints[2] + planePoints[3]) / 4f;

						float distance = Vector3.Distance(planeCenter, pointOnPlane);
						if(distance < closestDistance)
						{
							closestDistance = distance;
						}
					}
				}
			}

			return closestDistance;
		}

		//float DistanceFromMouseToPlane(List<Vector3> planeLines)
		//{
		//	if(planeLines.Count >= 4)
		//	{
		//		Ray mouseRay = myCamera.ScreenPointToRay(Input.mousePosition);
		//		Plane plane = new Plane(planeLines[0], planeLines[1], planeLines[2]);

		//		float distanceToPlane;
		//		if(plane.Raycast(mouseRay, out distanceToPlane))
		//		{
		//			Vector3 pointOnPlane = mouseRay.origin + (mouseRay.direction * distanceToPlane);
		//			Vector3 planeCenter = (planeLines[0] + planeLines[1] + planeLines[2] + planeLines[3]) / 4f;

		//			return Vector3.Distance(planeCenter, pointOnPlane);
		//		}
		//	}

		//	return float.MaxValue;
		//}

		#endregion Gizmo Interaction

		private void SetAxisInfo()
		{
			if(mainTargetRoot != null)
			{
				axisInfo.Set(mainTargetRoot, pivotPoint, GetProperTransformSpace());
			}
		}

		#region Drawing Preparation

		// This helps keep the size consistent no matter how far we are from it.
		public float GetDistanceMultiplier()
		{
			if(mainTargetRoot == null) return 0f;

			if(myCamera.orthographic) return Mathf.Max(.01f, myCamera.orthographicSize * 2f);
			return Mathf.Max(.01f, Mathf.Abs(ExtVector3.MagnitudeInDirection(pivotPoint - transform.position, myCamera.transform.forward)));
		}

		private void SetLines()
		{
			SetHandleLines();
			SetHandlePlanes();
			SetHandleTriangles();
			SetHandleSquares();
			SetCircles(GetAxisInfo(), circlesLines);
		}

		private void SetHandleLines()
		{
			handleLines.Clear();

			if(TranslatingTypeContains(TransformType.Move) || TranslatingTypeContains(TransformType.Scale))
			{
				float lineWidth = handleWidth * GetDistanceMultiplier();

				bool addX = false;
				bool addY = false;
				bool addZ = false;

				float xLineLength = 0;
				float yLineLength = 0;
				float zLineLength = 0;
				if(TranslatingTypeContains(TransformType.Move))
				{
					addX = IsAllowedAxis(TransformType.Move, Axis.X);
					addY = IsAllowedAxis(TransformType.Move, Axis.Y);
					addZ = IsAllowedAxis(TransformType.Move, Axis.Z);

					xLineLength = yLineLength = zLineLength = GetHandleLength(TransformType.Move);
				}
				else if(TranslatingTypeContains(TransformType.Scale))
				{
					addX = IsAllowedAxis(TransformType.Scale, Axis.X);
					addY = IsAllowedAxis(TransformType.Scale, Axis.Y);
					addZ = IsAllowedAxis(TransformType.Scale, Axis.Z);

					xLineLength = GetHandleLength(TransformType.Scale, Axis.X);
					yLineLength = GetHandleLength(TransformType.Scale, Axis.Y);
					zLineLength = GetHandleLength(TransformType.Scale, Axis.Z);
				}

				if (addX)
					AddQuads(pivotPoint, axisInfo.xDirection, axisInfo.yDirection, axisInfo.zDirection, xLineLength, lineWidth, handleLines.x);
				if (addY)
					AddQuads(pivotPoint, axisInfo.yDirection, axisInfo.xDirection, axisInfo.zDirection, yLineLength, lineWidth, handleLines.y);
				if (addZ)
					AddQuads(pivotPoint, axisInfo.zDirection, axisInfo.xDirection, axisInfo.yDirection, zLineLength, lineWidth, handleLines.z);
			}
		}

		private int AxisDirectionMultiplier(Vector3 direction, Vector3 otherDirection)
		{
			return ExtVector3.IsInDirection(direction, otherDirection) ? 1 : -1;
		}

		private void SetHandlePlanes()
		{
			handlePlanes.Clear();

			if(TranslatingTypeContains(TransformType.Move))
			{
				Vector3 pivotToCamera = myCamera.transform.position - pivotPoint;
				float cameraXSign = Mathf.Sign(Vector3.Dot(axisInfo.xDirection, pivotToCamera));
				float cameraYSign = Mathf.Sign(Vector3.Dot(axisInfo.yDirection, pivotToCamera));
				float cameraZSign = Mathf.Sign(Vector3.Dot(axisInfo.zDirection, pivotToCamera));

				float planeSize = this.planeSize;
				if(transformType == TransformType.All) { planeSize *= allMoveHandleLengthMultiplier; }
				planeSize *= GetDistanceMultiplier();

				Vector3 xDirection = (axisInfo.xDirection * planeSize) * cameraXSign;
				Vector3 yDirection = (axisInfo.yDirection * planeSize) * cameraYSign;
				Vector3 zDirection = (axisInfo.zDirection * planeSize) * cameraZSign;

				Vector3 xPlaneCenter = pivotPoint + (yDirection + zDirection);
				Vector3 yPlaneCenter = pivotPoint + (xDirection + zDirection);
				Vector3 zPlaneCenter = pivotPoint + (xDirection + yDirection);

				if (IsAllowedAxis(TransformType.Move, Axis.Y) && IsAllowedAxis(TransformType.Move, Axis.Z))
					AddQuad(xPlaneCenter, axisInfo.yDirection, axisInfo.zDirection, planeSize, handlePlanes.x);
				if (IsAllowedAxis(TransformType.Move, Axis.X) && IsAllowedAxis(TransformType.Move, Axis.Z))
					AddQuad(yPlaneCenter, axisInfo.xDirection, axisInfo.zDirection, planeSize, handlePlanes.y);
				if (IsAllowedAxis(TransformType.Move, Axis.X) && IsAllowedAxis(TransformType.Move, Axis.Y))
					AddQuad(zPlaneCenter, axisInfo.xDirection, axisInfo.yDirection, planeSize, handlePlanes.z);
			}
		}

		private void SetHandleTriangles()
		{
			handleTriangles.Clear();

			if(TranslatingTypeContains(TransformType.Move))
			{
				float triangleLength = triangleSize * GetDistanceMultiplier();

				// todo: Disable disallowed handles.

				if (IsAllowedAxis(TransformType.Move, Axis.X))
					AddTriangles(axisInfo.GetXAxisEnd(GetHandleLength(TransformType.Move)), axisInfo.xDirection, axisInfo.yDirection, axisInfo.zDirection, triangleLength, handleTriangles.x);
				if (IsAllowedAxis(TransformType.Move, Axis.Y))
					AddTriangles(axisInfo.GetYAxisEnd(GetHandleLength(TransformType.Move)), axisInfo.yDirection, axisInfo.xDirection, axisInfo.zDirection, triangleLength, handleTriangles.y);
				if (IsAllowedAxis(TransformType.Move, Axis.Z))
					AddTriangles(axisInfo.GetZAxisEnd(GetHandleLength(TransformType.Move)), axisInfo.zDirection, axisInfo.yDirection, axisInfo.xDirection, triangleLength, handleTriangles.z);
			}
		}

		private void AddTriangles(Vector3 axisEnd, Vector3 axisDirection, Vector3 axisOtherDirection1, Vector3 axisOtherDirection2, float size, List<Vector3> resultsBuffer)
		{
			Vector3 endPoint = axisEnd + (axisDirection * (size * 2f));
			Square baseSquare = GetBaseSquare(axisEnd, axisOtherDirection1, axisOtherDirection2, size / 2f);

			resultsBuffer.Add(baseSquare.bottomLeft);
			resultsBuffer.Add(baseSquare.topLeft);
			resultsBuffer.Add(baseSquare.topRight);
			resultsBuffer.Add(baseSquare.topLeft);
			resultsBuffer.Add(baseSquare.bottomRight);
			resultsBuffer.Add(baseSquare.topRight);

			for(int i = 0; i < 4; i++)
			{
				resultsBuffer.Add(baseSquare[i]);
				resultsBuffer.Add(baseSquare[i + 1]);
				resultsBuffer.Add(endPoint);
			}
		}

		private void SetHandleSquares()
		{
			handleSquares.Clear();

			if(TranslatingTypeContains(TransformType.Scale))
			{
				float boxSize = this.boxSize * GetDistanceMultiplier();

				// todo: Disable disallowed handles.

				if (IsAllowedAxis(TransformType.Scale, Axis.X))
					AddSquares(axisInfo.GetXAxisEnd(GetHandleLength(TransformType.Scale, Axis.X)), axisInfo.xDirection, axisInfo.yDirection, axisInfo.zDirection, boxSize, handleSquares.x);
				if (IsAllowedAxis(TransformType.Scale, Axis.Y))
					AddSquares(axisInfo.GetYAxisEnd(GetHandleLength(TransformType.Scale, Axis.Y)), axisInfo.yDirection, axisInfo.xDirection, axisInfo.zDirection, boxSize, handleSquares.y);
				if (IsAllowedAxis(TransformType.Scale, Axis.Z))
					AddSquares(axisInfo.GetZAxisEnd(GetHandleLength(TransformType.Scale, Axis.Z)), axisInfo.zDirection, axisInfo.xDirection, axisInfo.yDirection, boxSize, handleSquares.z);
				if (IsAllowedAxis(TransformType.Scale, Axis.Uniform))
					AddSquares(pivotPoint - (axisInfo.xDirection * (boxSize * .5f)), axisInfo.xDirection, axisInfo.yDirection, axisInfo.zDirection, boxSize, handleSquares.all);
			}
		}

		private void AddSquares(Vector3 axisStart, Vector3 axisDirection, Vector3 axisOtherDirection1, Vector3 axisOtherDirection2, float size, List<Vector3> resultsBuffer)
		{
			AddQuads(axisStart, axisDirection, axisOtherDirection1, axisOtherDirection2, size, size * .5f, resultsBuffer);
		}

		private void AddQuads(Vector3 axisStart, Vector3 axisDirection, Vector3 axisOtherDirection1, Vector3 axisOtherDirection2, float length, float width, List<Vector3> resultsBuffer)
		{
			Vector3 axisEnd = axisStart + (axisDirection * length);
			AddQuads(axisStart, axisEnd, axisOtherDirection1, axisOtherDirection2, width, resultsBuffer);
		}

		private void AddQuads(Vector3 axisStart, Vector3 axisEnd, Vector3 axisOtherDirection1, Vector3 axisOtherDirection2, float width, List<Vector3> resultsBuffer)
		{
			Square baseRectangle = GetBaseSquare(axisStart, axisOtherDirection1, axisOtherDirection2, width);
			Square baseRectangleEnd = GetBaseSquare(axisEnd, axisOtherDirection1, axisOtherDirection2, width);

			resultsBuffer.Add(baseRectangle.bottomLeft);
			resultsBuffer.Add(baseRectangle.topLeft);
			resultsBuffer.Add(baseRectangle.topRight);
			resultsBuffer.Add(baseRectangle.bottomRight);

			resultsBuffer.Add(baseRectangleEnd.bottomLeft);
			resultsBuffer.Add(baseRectangleEnd.topLeft);
			resultsBuffer.Add(baseRectangleEnd.topRight);
			resultsBuffer.Add(baseRectangleEnd.bottomRight);

			for(int i = 0; i < 4; i++)
			{
				resultsBuffer.Add(baseRectangle[i]);
				resultsBuffer.Add(baseRectangleEnd[i]);
				resultsBuffer.Add(baseRectangleEnd[i + 1]);
				resultsBuffer.Add(baseRectangle[i + 1]);
			}
		}

		private void AddQuad(Vector3 axisStart, Vector3 axisOtherDirection1, Vector3 axisOtherDirection2, float width, List<Vector3> resultsBuffer)
		{
			Square baseRectangle = GetBaseSquare(axisStart, axisOtherDirection1, axisOtherDirection2, width);

			resultsBuffer.Add(baseRectangle.bottomLeft);
			resultsBuffer.Add(baseRectangle.topLeft);
			resultsBuffer.Add(baseRectangle.topRight);
			resultsBuffer.Add(baseRectangle.bottomRight);
		}

		private Square GetBaseSquare(Vector3 axisEnd, Vector3 axisOtherDirection1, Vector3 axisOtherDirection2, float size)
		{
			Square square;
			Vector3 offsetUp = ((axisOtherDirection1 * size) + (axisOtherDirection2 * size));
			Vector3 offsetDown = ((axisOtherDirection1 * size) - (axisOtherDirection2 * size));
			// These might not really be the proper directions, as in the bottomLeft might not really be at the bottom left...
			square.bottomLeft = axisEnd + offsetDown;
			square.topLeft = axisEnd + offsetUp;
			square.bottomRight = axisEnd - offsetUp;
			square.topRight = axisEnd - offsetDown;
			return square;
		}

		private void SetCircles(AxisInfo axisInfo, AxisVectors axisVectors)
		{
			axisVectors.Clear();

			if(TranslatingTypeContains(TransformType.Rotate))
			{
				float circleLength = GetHandleLength(TransformType.Rotate);

				if (IsAllowedAxis(TransformType.Rotate, Axis.X))
					AddCircle(pivotPoint, axisInfo.xDirection, circleLength, axisVectors.x);
				if (IsAllowedAxis(TransformType.Rotate, Axis.Y))
					AddCircle(pivotPoint, axisInfo.yDirection, circleLength, axisVectors.y);
				if (IsAllowedAxis(TransformType.Rotate, Axis.Z))
					AddCircle(pivotPoint, axisInfo.zDirection, circleLength, axisVectors.z);
				if (IsAllowedAxis(TransformType.Rotate, Axis.Combined))
					AddCircle(pivotPoint, (pivotPoint - transform.position).normalized, circleLength, axisVectors.all, false);
			}
		}

		private void AddCircle(Vector3 origin, Vector3 axisDirection, float size, List<Vector3> resultsBuffer, bool depthTest = true)
		{
			Vector3 up = axisDirection.normalized * size;
			Vector3 forward = Vector3.Slerp(up, -up, .5f);
			Vector3 right = Vector3.Cross(up, forward).normalized * size;

			Matrix4x4 matrix = new Matrix4x4();

			matrix[0] = right.x;
			matrix[1] = right.y;
			matrix[2] = right.z;

			matrix[4] = up.x;
			matrix[5] = up.y;
			matrix[6] = up.z;

			matrix[8] = forward.x;
			matrix[9] = forward.y;
			matrix[10] = forward.z;

			Vector3 lastPoint = origin + matrix.MultiplyPoint3x4(new Vector3(Mathf.Cos(0), 0, Mathf.Sin(0)));
			Vector3 nextPoint = Vector3.zero;
			float multiplier = 360f / circleDetail;

			Plane plane = new Plane((transform.position - pivotPoint).normalized, pivotPoint);

			float circleHandleWidth = handleWidth * GetDistanceMultiplier();

			for(int i = 0; i < circleDetail + 1; i++)
			{
				nextPoint.x = Mathf.Cos((i * multiplier) * Mathf.Deg2Rad);
				nextPoint.z = Mathf.Sin((i * multiplier) * Mathf.Deg2Rad);
				nextPoint.y = 0;

				nextPoint = origin + matrix.MultiplyPoint3x4(nextPoint);

				if(!depthTest || plane.GetSide(lastPoint))
				{
					Vector3 centerPoint = (lastPoint + nextPoint) * .5f;
					Vector3 upDirection = (centerPoint - origin).normalized;
					AddQuads(lastPoint, nextPoint, upDirection, axisDirection, circleHandleWidth, resultsBuffer);
				}

				lastPoint = nextPoint;
			}
		}

		#endregion Drawing Preparation

		#region Drawing

		private void DrawLines(List<Vector3> lines, Color color)
		{
			if(lines.Count == 0) return;

			GL.Begin(GL.LINES);
			GL.Color(color);

			for(int i = 0; i < lines.Count; i += 2)
			{
				GL.Vertex(lines[i]);
				GL.Vertex(lines[i + 1]);
			}

			GL.End();
		}

		private void DrawTriangles(List<Vector3> lines, Color color)
		{
			if(lines.Count == 0) return;

			GL.Begin(GL.TRIANGLES);
			GL.Color(color);

			for(int i = 0; i < lines.Count; i += 3)
			{
				GL.Vertex(lines[i]);
				GL.Vertex(lines[i + 1]);
				GL.Vertex(lines[i + 2]);
			}

			GL.End();
		}

		private void DrawQuads(List<Vector3> lines, Color color)
		{
			if(lines.Count == 0) return;

			GL.Begin(GL.QUADS);
			GL.Color(color);

			for(int i = 0; i < lines.Count; i += 4)
			{
				GL.Vertex(lines[i]);
				GL.Vertex(lines[i + 1]);
				GL.Vertex(lines[i + 2]);
				GL.Vertex(lines[i + 3]);
			}

			GL.End();
		}

		private void DrawFilledCircle(List<Vector3> lines, Color color)
		{
			if(lines.Count == 0) return;

			Vector3 center = Vector3.zero;
			for(int i = 0; i < lines.Count; i++)
			{
				center += lines[i];
			}
			center /= lines.Count;

			GL.Begin(GL.TRIANGLES);
			GL.Color(color);

			for(int i = 0; i + 1 < lines.Count; i++)
			{
				GL.Vertex(lines[i]);
				GL.Vertex(lines[i + 1]);
				GL.Vertex(center);
			}

			GL.End();
		}

		#endregion Drawing

		private void SetMaterial()
		{
			if(lineMaterial == null)
			{
				lineMaterial = new Material(Shader.Find("Custom/Lines"));
			}
		}
	}
}
