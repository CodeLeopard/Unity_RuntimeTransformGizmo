// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


// This file is a modification of the original, which has the following copyright:
// Copyright (c) 2016 HiddenMonk
// The Original, licensed MIT can be found here: https://github.com/HiddenMonk/Unity3DRuntimeTransformGizmo



using System;

namespace RuntimeGizmos
{
	/// <summary>
	/// The space in which transformations occur.
	/// </summary>
	public enum TransformSpace
	{
		/// <summary>
		/// Relative to the World, the Global Coordinate System.
		/// </summary>
		Global,
		/// <summary>
		/// Relative to the Transform, the Local coordinate System.
		/// </summary>
		Local
	}

	/// <summary>
	/// The type of Gizmo to show.
	/// </summary>
	public enum TransformType
	{
		/// <summary>
		/// Show no gizmo at all, can be used by Extensions if they want to draw their own Gizmo.
		/// </summary>
		None,
		/// <summary>
		/// Show the movement Gizmo: Three arrows for X,Y,Z directions and 3 Planes.
		/// </summary>
		Move,
		/// <summary>
		/// Show the Rotation Gizmo: Three Circle handles
		/// </summary>
		Rotate,
		/// <summary>
		/// Show the Scale Gizmo: Three Arrows for X,Y,Z and a centered handle for Uniform Scale
		/// </summary>
		Scale,
		/// <summary>
		/// Show the Raycast Gizmo: Grab to move along the terrain.
		/// </summary>
		RayCast,
		/// <summary>
		/// Show Move, Rotate and Scale Gizmos simultaneously.
		/// </summary>
		All
	}

	/// <summary>
	/// The pivot around which transformations occur.
	/// </summary>
	[Flags]
	public enum TransformPivot : byte
	{
		None = 0,
		/// <summary>
		/// The pivot point is the origin of the transform.
		/// </summary>
		Origin = 1 << 0,
		/// <summary>
		/// The pivot is in the subjective Center of the Object (tree).
		/// </summary>
		Center = 1 << 1,
		/// <summary>
		/// All pivot modes are allowed.
		/// </summary>
		All = 0xFF
	}

	[Flags]
	public enum Axis : byte
	{
		None     = 0,
		X        = 1 << 0,
		Y        = 1 << 1,
		Z        = 1 << 2,
		/// <summary>
		/// Scale uniformly in all directions.
		/// </summary>
		Uniform  = 1 << 3, // For Scale. Uniform is a separate permission so that it can be allowed while individual axis aren't.
		/// <summary>
		/// Rotate all axis at once.
		/// </summary>
		Combined = (1 << 4) | X | Y | Z, // For Rotation. Combined rotates on all Axis so they must be allowed too.
		All      = X | Y | Z | Uniform | Combined,
	}

	// CenterType.All is the center of the current object Mesh or pivot if not Mesh and all its childrens mesh or pivot if no mesh.
	//	CenterType.All might give different results than Unity I think because Unity only counts empty GameObjects a little bit, as if they have less weight.
	// CenterType.Solo is the center of the current objects Mesh or pivot if no Mesh.
	// Unity seems to use Colliders first to use to find how much weight the object has or something to decide how much it effects the center,
	// but for now we only look at the Renderer.bounds.center, so expect some differences between Unity.
	public enum CenterType {All, Solo}

	// ScaleType.FromPoint acts as if you are using a parent Transform as your new pivot and transforming that parent instead of the child.
	// ScaleType.FromPointOffset acts as if you are scaling based on a point that is offset from the actual pivot. Its similar to Unity editor scaling in Center pivot mode (though a little inaccurate if object is skewed)
	public enum ScaleType {FromPoint, FromPointOffset}
}
